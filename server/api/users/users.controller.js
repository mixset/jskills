/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/users              ->  index
 */

'use strict';

var users = [
  {
    first_name: 'Emily',
    last_name: 'Clark',
    position: 'Customer Support Specialist',
    avatar: 'http://svgavatars.com/style/svg/08.svg',
    star: true,
    phone: '100666777',
    email: 'emily-clark@example.com',
    address: {
      street: 'Sopocka 13',
      city: 'Gdynia',
      country: 'Polska'
    }
  },
  {
    first_name: 'Kamil',
    last_name: 'Raben',
    position: 'Junior Tester',
    avatar: 'http://svgavatars.com/style/svg/09.svg',
    star: false,
    phone: null,
    email: 'kamil-raben@example.com',
    address: {
      street: 'Starogardzka',
      city: 'Tczew',
      country: 'Polska'
    }
  },
  {
    first_name: 'Joe',
    last_name: 'Doe',
    position: 'IT Developer',
    avatar: 'http://svgavatars.com/style/svg/11.svg',
    star: false,
    phone: '500600700',
    email: 'joe-doe@example.com',
    address: {
      street: 'Grunwaldzka 223',
      city: 'Gdańsk',
      country: 'Polska'
    }
  }
];

// Gets a list of Users
export function list(req, res) {
  res.json(users);
}

// Get User
export function index(req, res) {
  res.json({
    first_name: 'Joe',
    last_name: 'Doe',
    position: 'IT Developer',
    avatar: 'http://svgavatars.com/style/svg/11.svg',
    star: false,
    phone: '500 600 700',
    email: 'joe-doe@example.com',
    address: {
      street: 'Grunwaldzka 223',
      city: 'Gdańsk',
      country: 'Polska'
    }
  });
}
