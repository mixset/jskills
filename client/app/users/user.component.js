import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './users.routes';

export class UserController {

  user = {};

  /*@ngInject*/
  constructor() {
    this.user = this.data;
  }
}

export default angular.module('jskillsApp.users', [uiRouter])
  .config(routing)
  .component('user', {
    template: require('./details.html'),
    controller: UserController,
    bindings: {
      data: '<'
    }
  })
  .name;
