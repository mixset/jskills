'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('user', {
    url: '/users/:id',
    resolve: {
      user: ($http) => {
        return $http.get('/api/users/123')
          .then(response => {
            return response.data;
          });
      }
    },
    template: `<div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <h1 class="page-header">User: {{ $resolve.user.first_name }} {{ $resolve.user.last_name }}</h1>
                  </div>
                  <div class="col-lg-8">
                    <user data="$resolve.user"></user>
                  </div>
                </div>
              </div>`
  });
}
